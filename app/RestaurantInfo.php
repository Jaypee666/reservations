<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestaurantInfo extends Model
{
    protected $table = "restaurant_info";

    public static  function updateInfo ($request) {
        $info = RestaurantInfo::find($request->id);
        $info->start_time = $request->start_time;
        $info->end_time = $request->end_time;
        $info->save();
    }

    public static  function createInfo ($request) {
    	$info = new RestaurantInfo();
        $info->start_time = $request->start_time;
        $info->end_time = $request->end_time;
        $info->save();
    }

}
