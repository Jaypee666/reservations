<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestaurantTable extends Model
{
    protected $table = "restaurant_tables";

    public static  function createTable ($request) {
        $table = new RestaurantTable();
        $table->name = $request->name;
        $table->save();
    }
}
