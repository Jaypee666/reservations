<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reservation;
use App\RestaurantInfo;
use App\RestaurantTable;
use Carbon;

class ReservationsController extends Controller
{

    public function index() {
    	$tables = RestaurantTable::all();
        $info = RestaurantInfo::get();
        if($info->count() > 0) {
            $opening = date('h:i A', strtotime($info[0]->start_time));
            $closing = date('h:i A', strtotime($info[0]->end_time));
            $info = array(
                'start_time' => $opening, 
                'end_time' => $closing, 
            );
        }
       
    	return view('reservation', ['tables' => $tables, 'info' => $info]);
    }

    public function postReservation(Request $request) {
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required',
            'table_id' => 'required',
            'reservation_date' => 'required',
            'reservation_time' => 'required',
        ]);
    	if ($this->checkReservation($request)) {
    		Reservation::insertReservation($request);
            session()->flash('message_success', "Your reservation is pending, please wait for a response from your email.");
    		return redirect()->back();
    	} else {
            session()->flash('message_error', "Wrong Input or Table Already Taken.");
    		return redirect()->back();
    	}
    }

    public function checkReservation($request) {
    	$count = 0;
    	$now = Carbon::now();
        $now = $now->toDateString();
    	$time_now = strtotime($now);
    	$reservations = Reservation::where('table_id', $request->table_id)->get();
        $info = RestaurantInfo::get();
        $request->reservation_time = $request->reservation_time.":00";
        $reservation_time = strtotime($request->reservation_time);
        $start_time = strtotime($info[0]->start_time);
        $end_time = strtotime($info[0]->end_time);
    	if ($reservations) {
    		foreach ($reservations as $reservation) {
                $reserve = strtotime($reservation->reservation_time);
	    		if ($request->reservation_date == $reservation->reservation_date) {
	    			if ($reservation_time >= $start_time && $reservation_time < $end_time) {
                        if($reservation_time > $time_now) {
                            if ($reservation->status == "Accepted") {
                                if (($reservation_time - $reserve) <= 0 || ($reservation_time - $reserve) < 1) {
                                    $count++;
                                }
                            }
                        } else {
                            $count++;
                        }
	    			} else {
	    				$count++;
	    			}
	    		} else {
	    			if ($request->reservation_date < $now) {
	    				$count++;
	    			} else {
                        if($reservation_time >= $start_time && $reservation_time <= $end_time) {
                             if($reservation_time > $time_now) {
                                if ($reservation->status == "Accepted") {
                                    if (($reservation_time - $reserve) <= 0 || ($reservation_time - $reserve) < 1) {
                                        $count++;
                                    }
                                }
                            } else {
                                $count++;
                            }
                        } else {
                            $count++;
                        }
                    }
	    		}
	    	}
    	}
    	if($count > 0) {
    		return false;
    	} else {
    		return true;
    	}
    }

    public function sendEmail() {

    }
}
