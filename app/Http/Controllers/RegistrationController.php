<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class RegistrationController extends Controller
{

    public function index() {
    	return view('auth.register');
    }

    public function postRegister(Request $request) {
    	$this->validate($request,[
            'name' => 'required',
            'email' => 'required||unique:users||email',
            'password' => 'required',
        ]);
    	User::createUser($request);
    	session()->flash('message_success', "Succesfully Registered.");
        return redirect()->back();
    }
}
