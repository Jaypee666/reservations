<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RestaurantTable;
class ApiController extends Controller
{
    public function getTableImage(Request $request) {
    	$table = RestaurantTable::find($request->id);
    	return $table->name;
    }
}
