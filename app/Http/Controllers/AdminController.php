<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reservation;
use App\RestaurantInfo;
use App\User;
use App\RestaurantTable;

class AdminController extends Controller
{
	public function __construct()
    {
        $this->middleware('admin');
    }

    public function getReservations () {
    	$reservations = Reservation::get();
        $tables = RestaurantTable::get();
        $info = RestaurantInfo::get();
    	return view ('reservations', ['reservations' => $reservations, 'tables' => $tables, 'info' => $info]);
    }

    public function updateReservation(Request $request) {
    	Reservation::updateStatus($request);
        session()->flash('message_success', "Successfully updated reservation status.");
        return redirect()->back();
    }

    public function createTable(Request $request) {
        $this->validate($request,[
            'name' => 'required',
            'file' => 'required',
        ]);
        $request->file->move(public_path('uploads'), $request->name.'.jpg');
        RestaurantTable::createTable($request);
        session()->flash('message_success', "Successfully added new table.");
        return redirect()->back();
    }

    public function updateInfo(Request $request) {
        $this->validate($request,[
            'start_time' => 'required',
            'end_time' => 'required',
        ]);
        if($request->id) {
            RestaurantInfo::updateInfo($request);
        } else {
            RestaurantInfo::createInfo($request);
        }
        session()->flash('message_success', "Successfully updated restaurant opening and closing hours.");
        return redirect()->back();
    }

    public function getAdminRegistration() {
        return view('auth.adminregister');
    }

    public function createAdminRegistration(Request $request) {
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required||unique:users||email',
            'password' => 'required',
        ]);
        User::createUser($request);
        session()->flash('message_success', "Succesfully Registered.");
        return redirect()->back();
    }
}
