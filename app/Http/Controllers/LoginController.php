<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
class LoginController extends Controller
{

    public function index() {
    	if (Auth::guest()) {
            $user = User::first();
    		return view('auth.login', ['user' => $user]);
    	} else {
    		return redirect()->route('home');
    	}
    }

    public function postLogin(Request $request) {
    	$email = $request->email;
    	$password = $request->password;
    	if (Auth::attempt(['email' => $email, 'password' => $password])) {
    		return redirect()->intended('home');
    	} else {
            session()->flash('message_error', "Incorrect email or password.");
    		return redirect()->back();
    	}
    }

    public function logout() {
        Auth::logout();
        return redirect()->intended('/login');
    }
}
