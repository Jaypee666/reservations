<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $table = "reservations";

    public function table() {
        return $this->hasOne('App\RestaurantTable', 'id', 'table_id');
    }

    public static  function insertReservation ($request) {
    	$reservation = new Reservation();
    	$reservation->name = $request->name;
    	$reservation->email = $request->email;
    	$reservation->table_id = $request->table_id;
    	$reservation->reservation_date = $request->reservation_date;
    	$reservation->reservation_time = $request->reservation_time;
        $reservation->status = "Pending";
    	$reservation->save();
    }

    public static  function updateStatus ($request) {
        $reservation = Reservation::find($request->id);
        $reservation->status = $request->status;
        $reservation->save();
    }


}
