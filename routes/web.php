<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'home', 'uses' => 'LandingPageController@guestIndex']);
Route::get('/login', ['as' => 'login', 'uses' => 'LoginController@index']);
Route::post('/login', ['as' => 'post-login', 'uses' => 'LoginController@postLogin']);
Route::get('/register', ['as' => 'register', 'uses' => 'RegistrationController@index']);
Route::post('/register', ['as' => 'post-register', 'uses' => 'RegistrationController@postRegister']);
Route::post('/logout', ['as' => 'logout', 'uses' => 'LoginController@logout']);

Route::get('/home', ['as' => 'home', 'uses' => 'HomeController@index']);

Route::get('/reservation', ['as' => 'reservation', 'uses' => 'ReservationsController@index']);

Route::post('/reserve', ['as' => 'reserve', 'uses' => 'ReservationsController@postReservation']);

Route::get('/admin/reservations', ['as' => 'admin-reservations', 'uses' => 'AdminController@getReservations']);
Route::post('/admin/reservations/update', ['as' => 'admin-reservations-update', 'uses' => 'AdminController@updateReservation']);
Route::post('/admin/tables/update', ['as' => 'admin-tables-update', 'uses' => 'AdminController@createTable']);
Route::post('/admin/info/update', ['as' => 'admin-info-update', 'uses' => 'AdminController@updateInfo']);
Route::get('/admin/create', ['as' => 'admin-create', 'uses' => 'AdminController@getAdminRegistration']);
Route::post('/admin/create', ['as' => 'post-admin-create', 'uses' => 'AdminController@createAdminRegistration']);