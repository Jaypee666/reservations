<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Best Restaurant Reservation') }}</title>
        <!-- Styles -->
        <link href="/css/app.css" rel="stylesheet">
        <link href="{{ URL::asset('/css/main.css') }}" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="/js/reservation.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Gaegu" rel="stylesheet">
        <!-- Scripts -->
        <script>
        window.Best Restaurant Reservation = <?php echo json_encode([
        'csrfToken' => csrf_token(),
        ]); ?>
        </script>
    </head>
    <body>
        <div id="app">
            <nav class="navbar navbar-default navbar-static-top">
                <div class="container">
                    <div class="navbar-header">
                        <!-- Collapsed Hamburger -->
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                        <!-- Branding Image -->
                        <a class="navbar-brand" href="{{ url('/') }}">
                            {{ config('app.name', 'Best Restaurant Reservation') }}
                        </a>
                    </div>
                    <div class="collapse navbar-collapse" id="app-navbar-collapse">
                        <!-- Left Side Of Navbar -->
                        <ul class="nav navbar-nav">
                            &nbsp;
                        </ul>
                        <!-- Right Side Of Navbar -->
                        <ul class="nav navbar-nav navbar-right">
                            <!-- Authentication Links -->
                            @if (Auth::guest())
                            <li><a href="{{ url('/reservation') }}">Reservation</a></li>
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{ url('/register') }}">Register</a></li>
                            @else
                            @if (Auth::user()->level_id == 1)
                            <li><a href="{{ url('/reservation') }}">Reservation</a></li>
                            @else
                            <li><a href="{{ url('/admin/create') }}">Register Admin</a></li>
                            <li><a href="{{ url('/admin/reservations') }}">Reservations</a></li>
                            @endif
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>
                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </nav>
            @yield('content')
        </div>
        <!-- Scripts -->
        <script src="/js/app.js"></script>
        @if(Route::current()->getName() == 'home')
        <footer class="text-center">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-4">
                        <h4>Contact:</h4>
                        <p>nonimousgaming@gmail.com</p>
                    </div>
                    <div class="col-sm-4">
                        <h4>Agency:</h4>
                        <a href="https://www.usource.me/"><p>USource</p></a>
                    </div>
                    <div class="col-sm-4">
                        <h4>Address:</h4>
                        <p>Lipa City, Batangas, 4217, Philippines</p>
                    </div>
                </div>
                <div class="row">
                    Copyright ©John Paul L. Navarro 2018
                </div>
            </div>
        </footer>
        @endif
    </body>
</html>