@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if (Session::has('message_success'))
            <div class="alert alert-success">{{ Session::get('message_success') }}</div>
            @endif
            @if ($tables->count() == 0 || $info->count() == 0)
            <div class="alert alert-warning"><p>Kindly add tables and setup the opening and closing hours of the restaurant to enable the reservation feature.</p></div>
            @endif
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-heading">Reservations</div>
                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Table Name</th>
                            <th>Reservation Date</th>
                            <th>Reservation Time</th>
                            <th>Status</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach ($reservations as $reservation)
                            <tr>
                                <td> {{ $reservation->id }} </td>
                                <td> {{ $reservation->name }} </td>
                                <td> {{ $reservation->email }} </td>
                                <td> {{ $reservation->table->name }} </td>
                                <td> {{ $reservation->reservation_date }} </td>
                                <td> {{ $reservation->reservation_time }} </td>
                                <td> {{ $reservation->status }} </td>
                                <td>
                                    <form class="pull-left" method="POST" action="{{ url('/admin/reservations/update') }}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="id" value="{{ $reservation->id }}">
                                        <input type="hidden" name="status" value="Accepted">
                                        <button type="submit "class="btn btn-success">
                                        Accept
                                        </button>
                                    </form>
                                    <form class="pull-left" method="POST" action="{{ url('/admin/reservations/update') }}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="id" value="{{ $reservation->id }}">
                                        <input type="hidden" name="status" value="Declined">
                                        <button class="btn btn-danger">
                                        Decline
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @if($reservations->count() == 0)
                    <b><p>No Data Available</p></b>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Tables</div>
                <div class="panel-body">
                    <form enctype="multipart/form-data" method="POST" action="{{ url('/admin/tables/update') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="currentTables">Current Tables</label>
                            <select class="form-control" name="table_id" id="currentTables">
                                @foreach ($tables as $table)
                                <option value="{{ $table->id }}"> {{ $table->name }}</option>
                                @endforeach
                            </select>
                            <div class="form-group">
                                <label for="newTableName">Table Name</label>
                                <input type="text" class="form-control" name="name" id="newTableName">
                            </div>
                            <div class="form-group">
                                <label for="fileName">File Input</label>
                                <input type="file" class="form-control" name="file" id="fileName">
                            </div>
                        </div>
                        <button class="btn btn-success pull-right">
                        Add New Table
                        </button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Opening and Closing Hours</div>
                <div class="panel-body">
                    <form method="POST" action="{{ url('/admin/info/update') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="form-group">
                                <label for="startTime">Start Time</label>
                                <input type="time" class="form-control" name="start_time" id="startTime" value="{{ $info->count() > 0 ? $info[0]->start_time : '' }}">
                            </div>
                            <div class="form-group">
                                <label for="endTime">End Time</label>
                                <input type="time" class="form-control" name="end_time" id="endTime" value="{{ $info->count() > 0 ? $info[0]->end_time : '' }}">
                            </div>
                        </div>
                        <button class="btn btn-success pull-right">
                        Update Time
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection