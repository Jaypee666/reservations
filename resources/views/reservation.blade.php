@extends('layouts.app')
@section('content')
<div class="container">
    @if(sizeof($info) > 0 && $tables->count() > 0)
    <div class="row">
        <div class="col-md-2">
            <div class="panel panel-default">
                <div class="panel-heading">Table Preview</div>
                <div class="panel-body">
                    <div id="tableImageContainer">
                        <img class="img-responsive" src=" {{ URL::asset('/uploads/'.$tables[0]->name.'.jpg') }} "/>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Reservation</div>
                <div class="panel-body">
                    @if (Session::has('message_error'))
                    <div class="alert alert-danger">{{ Session::get('message_error') }}</div>
                    @elseif (Session::has('message_success'))
                    <div class="alert alert-success">{{ Session::get('message_success') }}</div>
                    @endif
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <form method="POST" action="{{ route('reserve') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="ReserveName">Full Name</label>
                            <input type="text" class="form-control" name="name" "id="ReserveName" placeholder="Ex: John D. Doe" value="{{ Auth::guest() == true ? '' : Auth::user()->name }}">
                        </div>
                        <div class="form-group">
                            <label for="ReserveEmail">Email address</label>
                            <input type="email" class="form-control" name="email" id="ReserveEmail" placeholder="Email" value="{{ Auth::guest() == true ? '' : Auth::user()->email }}">
                        </div>
                        <div class="form-group">
                            <label for="ReserveTable">Selected Table</label>
                            <select class="form-control" name="table_id" id="ReserveTable">
                                @foreach ($tables as $table)
                                <option value="{{ $table->id }}"> {{ $table->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="ReserveDate">Reservation Date</label>
                            <input type="date" class="form-control" name="reservation_date" id="ReserveDate">
                        </div>
                        <div class="form-group">
                            <label for="ReserveTime">Reservation Time</label>
                            <input type="time" class="form-control" name="reservation_time" id="ReserveTime">
                        </div>
                        <button type="submit" class="btn btn-success">Reserve</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="panel panel-default">
                <div class="panel-heading">Restaurant Info</div>
                <div class="panel-body">
                    <p><b>Openning Hour:</b> {{ $info['start_time'] }}</p>
                    <p><b>Closing Hour:</b> {{ $info['end_time'] }}</p>
                </div>
            </div>
        </div>
    </div>
    @else
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Restaurant Reservation</div>
                <div class="panel-body">
                    <p><b>The Reservation Feature hasn't been setup yet, please return later.</b></p>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
<script type="text/javascript">
    $(document).ready(function($) {
        $('#ReserveTable').on('focus change', function(event) {
            $.ajax({
                type:'POST',
                url:'/api/gettableimage',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    _token : '<?php echo csrf_token() ?>',
                    id : $(this).val()
                },
                success:function(data){
                    $('#tableImageContainer').empty();
                    $('#tableImageContainer').prepend('<img class="img-responsive" src=" {{ URL::asset('/uploads/') }}' +'/'+ data + '.jpg" />');
                }
            });
        });
    });
</script>
@endsection
