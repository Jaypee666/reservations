@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <header>
        	<h1>Sample Reservation Website For You To View!</h1>
        	<a href="{{ url('/reservation') }}"><button class="btn">Reserve A Seat Now</button></a>
        </header>
    </div>
    <div class="row text-center">
    	<h1 class="heading">This Website Is Built Using</h1>
    </div>
</div>
<div class="container home-page">
	<div class="row">
    	<div class="col-sm-4 text-center">
    		<img class="img-responsive logo" src="{{ asset('/images/laravel.png') }}">
    	</div>
    	<div class="col-sm-4 text-center">
    		<img class="img-responsive logo" src="{{ asset('/images/jquery.png') }}">
    	</div>
    	<div class="col-sm-4 text-center">
    		<img class="img-responsive logo" src="{{ asset('/images/html.png') }}">
    	</div>
    </div>
    <div class="row">
    	<div class="col-sm-4 text-center">
    		<h2>Laravel</h2>
    	</div>
    	<div class="col-sm-4 text-center">
    		<h2>JQuery</h2>
    	</div>
    	<div class="col-sm-4 text-center">
    		<h2>HTML 5</h2>
    	</div>
    </div>
    <div class="row text-center">
    	<h1 class="heading">Developer</h1>
    	<img class="logo" src="{{ asset('/images/developer.png') }}">
    	<h3>John Paul L. Navarro</h3>
    </div>
</div>
@endsection
